def is_pandigital(n):
    digits = [int(d) for d in str(n)]
    digits.sort()
    
    if digits == range(1, len(digits)+1):
        return True

    return False
