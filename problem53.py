from math import factorial

def problem53():
    count = 0
    for n in range(1, 101):
        for r in range(0, n+1):
            nCr = factorial(n) / (factorial(r)*factorial(n-r))

            if nCr > 10**6:
                count += 1

    return count
                                  
    
