from pandigital import is_pandigital
from itertools import permutations

def problem104():
    pand = {int(''.join(x)) for x in permutations('123456789')}
    print 'Got the pands'
    
    f1 = 1
    f2 = 1
    term = 2
    
    while True:
        n = str(f2)

        if len(n) >= 18:
            if is_pandigital(n[:9], pand) and is_pandigital(n[-9:], pand):
                return f2
        
        f1, f2 = f2, f1+f2
        term += 1

def is_pandigital(n, pand=set()):
    if n in pand:
        return True

    return False
    
