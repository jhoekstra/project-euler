##import random
##from itertools import permutations
##
##def problem41():
##    n = 3
##
##    while True:   
##        digits = ''.join([str(x) for x in list(range(1, n))])
##        perms = [''.join(p) for p in permutations(digits)]
##
##        for p in perms:
##            p = int(p)
##
##            if is_prime(p):
##                print p    
##        
##        n += 1
##
###Fermat's little theorem
##def is_prime(n):
##    for j in range(10):
##        a = random.randint(1, n-1)
##        b = a**(n-1)
##        if (b % n) != 1:
##            return False
##
##    print 'isPrime' 
##    return True

def problem41():
    primes = rwh_primes(90000000)
    for p in primes:
        if is_pandigital(p):
            print p

def rwh_primes(n):
    # http://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n-in-python/3035188#3035188
    """ Returns  a list of primes < n """
    sieve = [True] * n
    for i in xrange(3,int(n**0.5)+1,2):
        if sieve[i]:
            sieve[i*i::2*i]=[False]*((n-i*i-1)/(2*i)+1)
    return [2] + [i for i in xrange(3,n,2) if sieve[i]]

def is_pandigital(n):
    digits = [int(d) for d in str(n)]
    digits.sort()
    
    if digits == range(1, len(digits)+1):
        return True

    return False
    
    
    
