import random

def problem92():
    n = 1
    count = 0
    integer = str(n)
    intermediates = []
    n_map = {}
    
    
    while n < 10**7:
        summ = 0

        for d in integer:
            summ += (int(d)**2)
        
        if random.randint(0, 10**5) == 0:
            print n
        
        if summ == 89:
            count += 1
            n += 1
            integer = str(n)

            for i in intermediates:
                if not i in n_map:
                    n_map[i] = 89

            intermediates = []
            continue

        elif summ == 1:
            n += 1
            integer = str(n)
            for i in intermediates:
                if not i in n_map:
                    n_map[i] = 1

            intermediates = []
            continue

        elif summ in n_map:
            if n_map[summ] == 89:
                count += 1
                n += 1
                integer = str(n)
                intermediates = []
                continue

            else:
                n += 1
                integer = str(n)
                intermediates = []
                continue
                
    
        intermediates.append(summ)
        integer = str(summ)
        
    return count        
            
    
