def problem52():
    n = 1
    
    while True:
        x = [d for d in str(n)]
        x.sort()
        ''.join(x)
        
        if is_permissible(x, n):
            return n 

        n += 1

def is_permissible(x, n):
    for j in range(2,7):
        y = [d for d in str(j*n)]
        y.sort()
        ''.join(y)

        if not x == y:
            return False

    return True
        
