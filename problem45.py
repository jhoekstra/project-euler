def problem45():

    triangle = set()
    pentagonal = set()
    hexagonal = set()

    for n in range(10**6):
        triangle.add(n*(n+1)/2)
        pentagonal.add(n*(3*n-1)/2)
        hexagonal.add(n*(2*n-1))

    return (triangle & pentagonal) & hexagonal
