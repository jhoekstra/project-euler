def problem34():

    counter = 3
    fact_sum = 0
    
    while True:
        this_number = str(counter)

        factsum = 0
        for j in range(len(this_number)):
            fifth_power = fact(int(this_number[j]))
            factsum += fifth_power

        if counter == factsum:
            fact_sum += counter
            print fact_sum
            
        counter += 1

def fact(n):
    product = 1

    while n > 1:
        product *= n
        n -= 1

    return product
