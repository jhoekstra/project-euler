from itertools import permutations

def problem24():
    perms = list(permutations(range(10)))
    perms.sort()
    
    return perms[10**6-1]
