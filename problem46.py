import primes
import math

def problem46():
    while True:
        roof = 10000
        prms = primes.primes(roof)
        
        for j in range(2, roof):
            if not applies_CGconjecture(j, prms):
                return j

        roof *= 10
        print roof
        
def applies_CGconjecture(j, prms):

    if j not in prms and j % 2 != 0:
        for p in prms:
            if p > j:
                break

            maxsquare = int(math.ceil(j**0.5))
            for square in range(1, maxsquare):
                if p + 2*pow(square, 2) == j:
                    return True

        return False

    return True
    
    
                        
                
