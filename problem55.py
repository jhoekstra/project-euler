from palindromes import is_palindrome

def problem55():
    lychrel = 0
    step = 1
    
    while step < 10**4:       
        if is_lychrel(step):
            lychrel += 1

        step += 1

    return lychrel


def is_lychrel(n):
    iterations = 0
    
    while iterations < 50:
        n += int(str(n)[::-1])
        
        if is_palindrome(n):
            return False
        
        iterations += 1

    return True
