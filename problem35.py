import primes

def problem35():
    circular_primes = 0
    prime_list = set(primes.primes(1000000))
    
    for prime in prime_list:
        p = str(prime)
        perms = []
        for j in range(len(p)-1):
            p += p[0]
            p = p[1:]
            int_p = int(p)
            perms.append(int_p)
            
        if all_primes(perms, prime_list):
            circular_primes += 1

    return circular_primes

def all_primes(perms, prime_list):
    for perm in perms:
        if perm not in prime_list:
            return False

    return True
        
