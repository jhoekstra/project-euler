from primes import primes
from math import factorial

def problem381():
    prime_list = primes(10**8)
    prime_list.remove(2)
    prime_list.remove(3)
    
    k_list = range(1, 6)
    k_list.reverse()
    
    sum_S = 0
    last_n = 0
    last_fact = 1
    
    for prime in prime_list:
        S = 0
            
        for k in k_list:
            n = prime - k 
            this_fact = fast_factorial(n, last_n, last_fact)

            last_fact = this_fact
            last_n = n
            
            S += this_fact
        
        S %= prime
        sum_S += S

    return sum_S
    
def fast_factorial(n, last_n, last_fact):
    for j in range(last_n + 1, n + 1):
        last_fact *= j
        
    return last_fact

##def make_factorial_table(n):
##    table = {}
##    factorial = 1
##    
##    for j in range(n):
##        factorial *= 1
##        table[j] = factorial
##        
##    return table
##    
    
