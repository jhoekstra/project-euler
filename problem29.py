from sets import Set

def problem29():
    all_terms = Set()

    for a in range(2,101):
        for b in range(2, 101):
            all_terms.add(pow(a,b))

    return len(all_terms)
