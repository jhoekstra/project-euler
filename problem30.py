def problem30():

    counter = 10
    fifthpowernr_sum = 0
    
    while True:
        this_number = str(counter)

        powersum = 0
        for j in range(len(this_number)):
            fifth_power = pow(int(this_number[j]),5)
            powersum += fifth_power

        if counter == powersum:
            fifthpowernr_sum += counter
            print fifthpowernr_sum
            
        counter += 1
        
