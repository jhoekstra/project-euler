def problem31(amount=200, combs=0, exhausted_options=set()):
    print amount, combs

    #bottom of a branch
    if amount == 0:
        return combs
    
    #define coins and determine options, removing exhausted branches
    coins = [1, 2, 5, 10, 20, 50, 100, 200]
    options = set([coin for coin in coins if coin <= amount])
    options = options.difference(exhausted_options)

    print options
    
    #all branches explored?
    if len(options) <= 1:
        return combs
    
    #add this option to the list of exhausted options
    this_option = options.pop()
    exhausted_options.add(this_option)

    #sum the options for each coin
    return problem31(amount, combs, exhausted_options) \
           + problem31(amount - this_option, combs+1)
        
        

    
