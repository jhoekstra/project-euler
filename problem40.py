def problem40():
    integer = 0
    digits = 0
    expression = 1
    threshold = 1
    
    while True:
        #escape condition
        if threshold == 10000000:
            return expression
        
        #body
        integer += 1
        this_int = str(integer)

        for digit in this_int:
            digits += 1
        
            if digits == threshold:
                threshold *= 10
                expression *= int(digit)

        
