import re
from decimal import *

def problem26():

    longest_cycle = 0
    nominator = None
    
    for j in range(1, 1000):
        print 'Nominator :', j
        
        #remove 0.0+
        getcontext().prec = 100
        fraction = str(Decimal(1.0/j))[2:]
        
        n = len(fraction)/2
        
        while True:

            #out of digits
            if n == 1:
                break

            #cycle found
            if fraction[0:n] == fraction [n:2*n] and fraction[0:n-1] != fraction [n-1:2*(n-1)]:
                
                if n > longest_cycle:
                    longest_cycle = n
                    nominator = j
                    break

                else:
                    break
                
            n -= 1

    print 'Nominator: ', nominator
    print 'Longest cycle: ', longest_cycle
