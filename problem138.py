from math import sqrt

def problem138():
    summ = 0
    count = 0
    b = 2
    
    #h = sqrt(L**2 - (0.5*b)**2)
    #h**2 + (0.5*b)**2 = L**2 
    
    while True:
        h1 = b - 1
        h2 = b + 1
        
        L1 = sqrt(h1**2 + (0.5*b)**2)
        L2 = sqrt(h2**2 + (0.5*b)**2)
        
        if L1 % 1 == 0.0:
            summ += L1
            count += 1
            print count

            if count == 12:
                return summ

        if L2 % 1 == 0.0:
            summ += L2
            count += 1
            print count

            if count == 12:
                return summ

        b += 1

 
