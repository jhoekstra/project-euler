from primes import primes

def problem187():
    
    prms = primes(10**8)
    two_primes = []

    for p1 in prms:
        for p2 in prms:
            n = p1*p2

            if n > 10**8:
                break

            two_primes.append(n)

    return len(set(two_primes))
            
                

    
    

      
    
