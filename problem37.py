from primes import primes

def problem37():
    prms = primes(10**6)
    Sprms = set(prms)
    
    prms.remove(2)
    prms.remove(3)
    prms.remove(5)
    prms.remove(7)
    
    truncatable_primes = []
    
    for prime in prms:
        if is_truncatable(prime, Sprms):
            truncatable_primes.append(prime)

            if len(truncatable_primes) == 11:
                return sum(truncatable_primes) 

            print truncatable_primes
            
    return 'Need more primes'

def is_truncatable(prime, Sprms):
    str_prime = str(prime)
        
    for j in range(1, len(str_prime)):
        if not is_prime(int(str_prime[:j]), Sprms):
            return False
        
        if not is_prime(int(str_prime[j:]), Sprms):
            return False

    return True
    
def is_prime(n, Sprms):
    if n in Sprms:
        return True

    return False
