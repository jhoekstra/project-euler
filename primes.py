import random

def primes(n):
    # http://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n-in-python/3035188#3035188
    """ Returns  a list of primes < n """
    sieve = [True] * n
    for i in xrange(3,int(n**0.5)+1,2):
        if sieve[i]:
            sieve[i*i::2*i]=[False]*((n-i*i-1)/(2*i)+1)
    return [2] + [i for i in xrange(3,n,2) if sieve[i]]

def is_prime(n, prms=set()):
    if n < 2:
        return False

    if n in prms:
        return True
    
    for j in range(10):
        a = random.randint(1, n-1)
        b = a**(n-1)
        if (b % n) != 1:
            return False

    return True

def is_truncatable(prime):
    assert(prime > 10)

    str_prime = str(prime)
        
    for j in range(1, len(str_prime)):
        if not is_prime(int(str_prime[:j])):
            return False
        
        if not is_prime(int(str_prime[j:])):
            return False

    return True
