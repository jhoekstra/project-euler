def problem44():
    pentagonals = [1]
    diffs = []
    
    n = 2
    while True:
        new_pent = n*(3*n-1)/2
        
        for pent in pentagonals:
            diff = abs(new_pent - pent)
            summ = new_pent + pent
            
            if diff in pentagonals and is_pent(summ, n+1):
                diffs.append(diff)
                print min(diffs)

        pentagonals.append(new_pent)
        n += 1
        print n
        
def is_pent(summ, n):
    while True:
        future_pent = n*(3*n-1)/2
        
        if future_pent == summ:
            print 'returned True'
            return True
            
        elif future_pent > summ:
            return False

        n += 1


##def problem44():
##    pentagonals = [1]
##    diffs = []
##    
##    n = 2
##    while True:
##        new_pent = n*(3*n-1)/2
##        
##        for pent in pentagonals:
##            diff = abs(new_pent - pent)
##            summ = new_pent + pent
##            
##            if is_pent(diff) and is_pent(summ):
##                diffs.append(diff)
##                print n
##                print min(diffs)
##
##        pentagonals.append(new_pent)
##        n += 1
##        
##def is_pent(n):
##    return pow(1 + 24*n, 0.5) % 6 == 5
##
