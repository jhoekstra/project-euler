def problem63():
    count = 0
    power = 1

    while True:
        base = 1
        
        while True:
            n = str(base**power)
            
            if len(n) == power:
                count += 1
                print count

            elif len(n) > power:
                break

            base += 1
            
        power += 1
        print 'Power: ', power
