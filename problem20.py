import math

def problem20():
    n = math.factorial(100)

    summ = 0
    for digit in str(n):
        summ += int(digit)

    return summ
