import math

def problem100():
##
##    total_discs = 10**12
##    blue_discs = 1

##    while True:
##        prob_B = 1.0*blue_discs / total_discs
##        prob_B_condB = 1.0*(blue_discs-1) / (total_discs-1)
##        prob_BB = prob_B * prob_B_condB
##    
##        if prob_BB == 0.5:
##            return blue_discs
##
##        elif prob_BB < 0.5:
##            blue_discs *= 1.1
##            blue_discs = math.ceil(blue_discs)
##
##        else:
##            blue_discs /= 1.05
##            blue_discs = math.ceil(blue_discs)
##
##        #print blue_discs
##    

    total = 10**12
    n = 0.5*(total**2 - total)
    blue = 1
    stepsize = 2
    attempts = []
    
    while True:
        eq = blue*(blue-1) - n
        attempts.append(eq)

        if eq == 0:
            return blue
        
        #adjust stepsize
        if len(attempts) > 4:
            last_attempts = attempts[-4:]
            positive = 0
            
            for a in last_attempts:
                if a > 0:
                    positive += 1

            if positive == 2:
                stepsize *= 0.95
            
            elif positive > 2:
                stepsize *= 0.9
                print stepsize
                print 'Blue = ', blue
            
            elif positive < 2:
                stepsize *= 1.1
                print stepsize
                print 'Blue = ', blue
                
        #body
        if eq == 0:
            return blue

        elif eq < 0:
            blue *= stepsize
            blue = math.ceil(blue)
            
        elif eq > 0:
            blue /= stepsize
            blue = math.ceil(blue)
    
