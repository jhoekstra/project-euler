def problem25():
    f1 = 1
    f2 = 1
    n = 3

    while True:
        f1, f2 = f2, f1 + f2

        if len(str(f2)) > 999:
            return n

        n += 1
